# Japan Notes

## Things to do

| Place | Location | Price | Notes |
|--------------|-----------|------------|---|
| [Pokémon Store](https://www.pokemon.co.jp/shop/en/) | Basically everywhere | ££ | | 
| [Cup Noodle Museum](https://www.cupnoodles-museum.jp/en/osaka_ikeda/) | Osaka (There's another elsewhere but I went here)  | £ | |
| [Fushimi Inari Taisha (Famous mountain temple)](https://www.google.com/maps/place/Fushimi+Inari+Taisha/@34.9676989,135.7766127,17z/data=!3m1!4b1!4m6!3m5!1s0x60010f153d2e6d21:0x7b1aca1c753ae2e9!8m2!3d34.9676945!4d135.7791876!16zL20vMDVsZHJt?entry=ttu) | Kyoto | £ | |
| [Nijo-jo Castle](https://nijo-jocastle.city.kyoto.lg.jp/?lang=en) | Kyoto | £ | Cool castle/temple. Place where the first and last shogun were sworn in/removed from office.
| [Universal Studios Japan](https://www.usj.co.jp/web/en/us)| Osaka | £££ | If you've ever been to US or Disney World, it's like that. Not a lot of actual rollercoasters, but like simulator rides. Good though. We pre-booked this by about 3 months |
| [Drunken Clam Karoke Bar](https://www.google.com/maps/place/The+Drunken+Clam/@34.6703008,135.5023317,17z/data=!3m1!5s0x6000e7141328b193:0xcc8a27905e510a01!4m15!1m8!3m7!1s0x6000e7146cc75fe7:0xb1b46e97deb33960!2sThe+Drunken+Clam!8m2!3d34.6703005!4d135.5023737!10e5!16s%2Fg%2F11b6dxqfbr!3m5!1s0x6000e7146cc75fe7:0xb1b46e97deb33960!8m2!3d34.6703005!4d135.5023737!16s%2Fg%2F11b6dxqfbr?authuser=0&hl=en&entry=ttu) | Osaka | £ | This place is very friendly to foreigners |
| [Akihabara](https://www.google.com/maps/place/Akihabara,+Taito+City,+Tokyo+110-0006,+Japan/@35.7021768,139.7719771,17z/data=!3m1!4b1!4m6!3m5!1s0x60188ea73ea6f4ff:0x5eb9f1e50fe061e3!8m2!3d35.7022589!4d139.7744733!16s%2Fg%2F11bc6tf5hn?authuser=0&hl=en&entry=ttu) | Tokyo | ££ | Anime and nerd central, you know this
| [Shibuya Crossing](https://www.google.com/maps/place/Shibuya+Scramble+Crossing/@35.6594439,139.7022577,19.71z/data=!3m1!5s0x60188b57f546295f:0x486cece41a7b21b0!4m6!3m5!1s0x60188bcaeb0cd12b:0x20e563a2e0aec969!8m2!3d35.659482!4d139.7005596!16s%2Fg%2F11shy4scrj?authuser=0&hl=en&entry=ttu) | Tokyo | 0 | It's a big road crossing. It's cool |
| [Tokyo SkyTree](https://www.tokyo-skytree.jp/en/) | Tokyo | ££ | Big observation tower that lets you basically see all of Tokyo. Very cool views, especially at sunset |
| [TeamLabs Planets](https://www.teamlab.art/e/planets/) | Tokyo | £ | Sensory museum. Very cool. We prebooked for like a couple months, maybe 3?
| [Mishima SkyWalk](http://mishima-skywalk.jp/) | Mishima | ££ | Cool mountain area with loads of activities and view of Mt. Fuji. Have blue ice cream too, very good
| [Ninja/Samurai Museum](https://mai-ko.com/samurai/tokyo.html) | Tokyo (There's another in Kyoto but I went here) | £

## Things to eat

| Place | Location | Price | Notes |
|--------------|-----------|------------|---|
| [Ichiran](https://en.ichiran.com/index.php) | Basically everywhere | £
| [Toriyasu](https://www.google.com/maps/place/Toriyasu/@35.0124512,135.7597082,17z/data=!4m15!1m8!3m7!1s0x600108866fe547e3:0x891916eacf59142c!2sToriyasu!8m2!3d35.0123803!4d135.7598561!10e5!16s%2Fg%2F1th7y7h8!3m5!1s0x600108866fe547e3:0x891916eacf59142c!8m2!3d35.0123803!4d135.7598561!16s%2Fg%2F1th7y7h8?authuser=0&hl=en&entry=ttu) | Kyoto | £
| [Bifuteki Kawamura Premium Kitashinchi Restaurant](https://www.bifteck.co.jp/en/restaurant/kitashinchi/) | Osaka | £££££ | Kobe beef in Kobe. Best steak I've ever had |
| [Torikizoku](https://www.google.com/maps/place/Torikizoku+Dotombori/@34.6688402,135.4992161,18z/data=!3m2!4b1!5s0x6000e71487331aff:0xf123697a23a0299!4m6!3m5!1s0x6000e71480711753:0x7a17be9b9db3f612!8m2!3d34.668838!4d135.5016515!16s%2Fg%2F1tfg7kf0?authuser=0&hl=en&entry=ttu) | Franchise in a bunch of places, but I had in Osaka | ££ | Yakitori restaurant that's relatively cheap and real good |
| [Buta-Daigaku Shinbashi](https://butadaigaku.jp/) | Tokyo | £ | Butadon place. Very good and cheap. We went here twice |

**The above are the standouts, but everything there fucking bangs. Put it all in your mouth hole**

## Things to know

| Place | Location | Price | Notes |
|--------------|-----------|------------|---|
| [ICOCA Card](https://www.westjr.co.jp/global/en/howto/icoca/) | Basically everywhere | £ | It's a oyster card for all of Japan. This is an essential
| [Shinkansen Tickets](https://global.jr-central.co.jp/en/onlinebooking/contents/shinkansen/) | Anywhere the Shinkansen goes | £££ | These are a lot more complicated than it needs to be. You need 2 tickets for some reason. I'd book this online as much as you can as I imagine that solves this issue. Getting them on the day is very confusing |
| Pre-paid SIM | Airport when you land | ££ | Worth to get so you have internet basically everywhere at 4G. Even on trains
